import boto3
import os
from dotenv import dotenv_values, load_dotenv
bucket_name = 'pabdkrylova'
load_dotenv('.env')

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
    #aws_access_key_id = 'YCAJEesX8NQMsKfEu17YMCbjb',
    #aws_secret_access_key = 'YCMZsCMCf7XxGEFFXsbb74-o8vko0-7HDgEa8JLu'
)

# Создать новый бакет
#s3.create_bucket(Bucket='pabdkrylova')

# Загрузить объекты в бакет

## Из строки
#s3.put_object(Bucket='pabdkrylova', Key='object_name', Body='TEST', StorageClass='COLD')

## Из файла
#s3.upload_file('this_script.py', 'bucket-name', 'py_script.py')
s3.upload_file('models/model_1.joblib', 'pabdkrylova', 'model_1.joblib')

# Получить список объектов в бакете
for key in s3.list_objects(Bucket='pabdkrylova')['Contents']:
    print(key['Key'])

# Удалить несколько объектов
#forDeletion = [{'Key':'object_name'}, {'Key':'script/py_script.py'}]
#response = s3.delete_objects(Bucket='pabdkrylova', Delete={'Objects': forDeletion})

# Получить объект
#get_object_response = s3.get_object(Bucket='pabdkrylova',Key='py_script.py')
#print(get_object_response['Body'].read())