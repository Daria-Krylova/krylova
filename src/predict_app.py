"""House price prediction service"""
from joblib import load

from flask import Flask, request
import yaml
from yaml import SafeLoader


app = Flask(__name__)
config =yaml.load(open('params/predictapp.yaml'), yaml.SafeLoader)
model = load(config['model_path'])


def get_model():
    """Build model and load weights from disk"""
    return sum


@app.route("/")
def home():
    """Dummy service"""
    f_1 = request.args.get("GrLivArea")
    f_2 = request.args.get("f2")
    f_3 = request.args.get("f3")
    f_1, f_2, f_3 = int(f_1), int(f_2), int(f_3)
    #model = get_model()
    result = model.predict([[f_1]])[0]
    return f'{round(result/1000)} тыс. руб'


if __name__ == "__main__":
    app.run()
