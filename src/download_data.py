from requests import get

def download(url, file_name):
    with open(file_name, 'wb') as file:
        response = get(url)
        file.write(response.content)

if __name__ == '__main__':
    url = 'https://storage.yandexcloud.net/pabdkrylova/cian_parsing_result_sale_1_42_moskva_01_Jun_2023_21_44_30_521694.csv'
    file_name = 'data/raw/cian'+ url.split('/')[-1]
    download(url, file_name)